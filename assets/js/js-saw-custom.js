{{ $platform_list := slice }}
{{ range site.Data.datalist.platform }}
  {{ range $i, $k := . }}
    {{ $platform_list = $platform_list | append $i }}
  {{ end }}
{{ end }}

{{/* $platform_list = ["Desktop", "Mobile-Tablet", "Browser", "TV", "Gadget", "Language", "Software", "Other"] */}}

{{/* Keeping hardware_get_it_list separate so as to allow flexibility in future */}}
{{ $hardware_get_it_list := $platform_list }}

// declaring src_present as global variable; it determines whether to print `source` into output. src_present is turned `true` if any infocard has a source present.
let src_present = false;

// reusable variables that assume value relative to their enviroment
let 
    // rowNum is a general variable used to keep count of current row
    rowNum,
    // markup is used to store current markup
    markup,
    // val is used to store value of current infocard output
    val;

/* none needed after having consolidated and simplified the logic for recs handling in infocards
// variable to keep track of rows(records); to be converted into a single object 'recs_num'
let 
    categories_recs_num = 1,
    aliases_recs_num = 1,
    social_recs_num = 1,
    features_recs_num = 1,
    quick_links_recs_num = 1,
    get_it_recs_num = 1,
      // create an object with format - rowNumber(string) : platformNumber(integer)
      platform_get_it_recs_num = { "1": 1 },
      platform_get_it_number,
    rating_recs_num = 1,
    sysreq_recs_num,
      sysreq_navs_total = $(".sysreq_nav .sysreq_table_div").length,
    license_recs_num = 1,
    developer_recs_num = 1,
    written_in_recs_num = 1;
*/

// variable to cache all 'chk' (primary checkbox) elements
/*let chk = {
    description: $("#description_chk"),
    ...
    };
    */

//
let image_name;

// https://stackoverflow.com/a/19872985
// used for standardizing file names, of images or software files
String.prototype.filenamify = function() {
  return this
    .toLowerCase()
    .replaceAll('(', '')
    .replaceAll(')', '')
    .replaceAll(' ', '-')
    .replaceAll('+', '-plus')
    .replaceAll('#', '-sharp')
    .replaceAll('.', 'dot')
    .replaceAll('[^a-zA-Z0-9]+', '-');
}

// all global function definitions
// yaml output functions
// title
function title() {
  val = "title: " + "\"" + $(".infocard-title input.title").val() + "\"\n\n";
  return val;
}
// description
function description() {
  if ($("#description_chk").is(":checked")) {
    val = "description: " + "\"" + $(".infocard-description textarea.description").val().replace(/\n/g, "  ").replace(/\"/g, "\'") + "\"\n\n";
    // .replace function is used to prevent anomalies in main website due to new line(\n) or quotes(")
    return val;
  } else if ($(".infocard-description textarea.description").val()) {
    alert("Checkbox for 'Description' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// publishDate
function publishDate() {
  if ($("#publishDate_chk").is(":checked")) {
    val = "publishDate: " + "\"" + $(".infocard-publishDate textarea.publishDate").val() + "\"\n";
    val = val + "\n";
    return val;
  } else if ($(".infocard-publishDate textarea.publishDate").val()) {
    alert("Checkbox for 'Publish Date' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// keywords
function keywords() {
  if ($("#keywords_chk").is(":checked")) {
    val = "keywords: " + "[\"" + $(".infocard-keywords textarea.keywords").val().replace(/,\s/g, "\", \"") + "\"]\n";
    val = val + "\n";
    return val;
  } else if ($(".infocard-keywords textarea.keywords").val()) {
    alert("Checkbox for 'Keywords' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// image
function image() {
  if ($("#image_chk").is(":checked")) {
    
    val = "image: " + "\"";
    
    if ($("input.default_image_rdo").is(":checked")) {
      val = val + "https://img.softorage.com/software-logo/";
      
      if ($("#filename_chk").is(":checked")) {
        val = val + $("#filename").value.filenamify() + "." + $("input.default-image-extension").val();
      } else {
        val = val + $(".infocard-title input.title").val().filenamify() + "." + $("input.default-image-extension").val();
      }
    } else {
      val = val + $(".infocard-image input.image").val();
    }
    
    val = val + "\"\n" + "\n";
    
    return val;
  } else if ($("input.default_image_rdo").is(":checked") || $(".infocard-image input.image").val()) {
    alert("Checkbox for 'Image' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// status
function status() {
  if ($("#status_chk").is(":checked")) {
    val = "status: " + "\"" + $(".infocard-status select.status").val() + "\"\n";
    val = val + "\n";
    return val;
  } else if ($(".infocard-status select.status").val()) {
    alert("Checkbox for 'Status' is unchecked, but dropdown is not empty. Unchecking the checkbox defaults the value to 'Unknown'. It is recommended to check the checkbox and select apt value from dropdown (which may as well be 'Unknown').");
    return "";
  } else {
    return "";
  }
}
function src_status() {
  if ($("#status_chk").is(":checked") && $(".infocard-status input.src_status_chk").is(":checked")) {
    val = "  status: " + "[\"" + $(".infocard-status textarea.src_status").val().replace(/,\s/g, "\", \"") + "\"]\n";
    return val;
  } else if ($(".infocard-status textarea.src_status").val()) {
    alert("Checkbox for 'Source for Status' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
function src_status_present() {
  if ($("#status_chk").is(":checked") && $(".infocard-status input.src_status_chk").is(":checked")) {
    return true;
  } else {
    return false;
  }
}
// website
function website() {
  if ($("#website_chk").is(":checked")) {
    val = "website: " + "\"" + $(".infocard-website input.website").val() + "\"\n";
    val = val + "\n";
    return val;
  } else if ($(".infocard-website input.website").val()) {
    alert("Checkbox for 'Website' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// get it
function get_it() {
  if ($("#get_it_chk").is(":checked") && $(".infocard-get_it input.get_it_rec_chk[type='checkbox']").length > 0) {
    //checbox for sys_req is ticked
    val = "get_it:\n";
    
    $(".infocard-get_it tbody tr").each(function() {      
      val = val + 
      "  - " + "from: \"" + $(this).find("input.name_get_it").val();
      
      if ($(this).find("input.authentic_get_it").is(":checked") && !$(this).find("input.name_get_it").val().includes("Authentic")) {
        val = val + " | Authentic";
      }
      
      val = val + "\"\n";
      
      val = val + "    " + "url: \"" + $(this).find("textarea.url_get_it").val() + "\"\n";
      
      let col2 = $(this).find("td")[1];
      
      // if there are multiple pairs of platforms, then include platform field in output OR
      // (if there is a single platform specified and) if value of 'name input field' is not 'empty', then include platform field in output
      // the condition in () above is redundant hence not specified in if statement beow
      if (($(col2).find(".form-group").length > 1) || ($(col2).find(".form-group").find("input.platform_name_get_it").eq(0).val().replace(/\s/g, "") != "")) {
        val = val + "    " + "platform:\n";
      }
      
      //
      $(col2).find(".form-group").each(function(i) {
        // check once if value of 'name input field' is not 'empty', and only then proceed, so that having no value for 'name input field' acts as if no platform is specified there
        if ($(this).find("input.platform_name_get_it").eq(0).val().replace(/\s/g, "") != "") {
          val = val +
          "      - " + "name: \"" + $(this).find("input.platform_name_get_it").val() + "\"\n";
          
          // hardware : desktop/laptop or mobile/tab
          if ($(this).find("select.hardware_get_it_list").eq(0).val()) {
            val = val +
            "        " + "hardware: ";
            val = val + "\"" + $(this).find("select.hardware_get_it_list").eq(0).val() + "\"";
            val = val + "\n";
          }
          
          // architecture
          if ($(this).find("input.platform_arch32_get_it_chk").eq(0).is(":checked") || $(this).find("input.platform_arch64_get_it_chk").eq(0).is(":checked")) {
            val = val +
            "        " + "arch: [";
            
            if ($(this).find("input.platform_arch32_get_it_chk").eq(0).is(":checked")) {
              val = val + "\"x32\"";
              if ($(this).find("input.platform_arch64_get_it_chk").eq(0).is(":checked")) {
                val = val + ", ";
              }
            }
            if ($(this).find("input.platform_arch64_get_it_chk").eq(0).is(":checked")) {
              val = val + "\"x64\"";
            }
            val = val + "]\n";
          }
          // official / unofficial
          if ($(this).find("input.platform_type_get_it_chk").eq(0).is(":checked")) {
            val = val +
            "        " + "official: " + "true\n";
          } else {
            val = val +
            "        " + "official: " + "false\n";
          }
          // remarks if any, eg. portable
          if ($(this).find("input.platform_remarks_get_it").val().replace(/\n/g, "").replace(/\s/g, "") != "" ) { // .replace function is used to prevent anomalies in main website due to mere new line(\n) or spaces, which might occasionally go unnoticed
            val = val + "        " + "remarks: \"" + $(this).find("input.platform_remarks_get_it").val() + "\"\n";
          }
          
        }
      });
    });
    
    val = val + "\n";
    return val;
  } else if ($(".infocard-get_it input.get_it_rec_chk[type='checkbox']").length > 1) {
    alert("Checkbox for 'Get it' is unchecked, but there are multiple items listed. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// rating
function rating() {
  if ($("#rating_chk").is(":checked") && $(".infocard-rating input.rating_rec_chk[type='checkbox']").length > 0) {
    val = "rating:\n";
    $(".infocard-rating tbody tr").each(function(i) {
      val = val +
      "  - name: \"" + $(this).find("input.name_rating").val() + "\"\n" +
      "    rate: [" + $(this).find("input.rate_rating").val() + ", " + $(this).find("input.outof_rating").val() + "]\n";
      if ($(this).find("input.users_rating_rdo").is(":checked")) {
        val = val + "    num: " + $(this).find("input.num_rating").val() + "\n";
      }
      if ($(this).find("input.remarks_rating").val().replace(/\n/g, "").replace(/\s/g, "") != "") { // .replace function is used to prevent anomalies in main website due to only new line(\n) or spaces
        val = val + "    remarks: \"" + $(this).find("input.remarks_rating").val() + "\"\n";
      }
    });
    val = val + "\n";
    return val;
  } else if ($(".infocard-rating input.rating_rec_chk[type='checkbox']").length > 1) {
    alert("Checkbox for 'Rating' is unchecked, but there are multiple items listed. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
function src_rating() {
  if ($("#rating_chk").is(":checked") && $(".infocard-rating input.src_rating_chk[type='checkbox']:checked").length > 0) {
    val = "  rating:\n";
    $(".infocard-rating tbody tr").each(function(i) {
      let rating_based_on = "expert";
      if ($(this).find("input.users_rating_rdo").is(":checked")) {
        rating_based_on = "user";
      }
      val = val + 
      "    - " + "name: \"" + $(this).find("input.name_rating").val() + "\"\n" + 
      "      " + "type: \"" + rating_based_on + "\"\n" + 
      "      " + "url: \"" + $(this).find("textarea.src_rating").val() + "\"\n";
      if ($(this).find("input.remarks_rating").val().replace(/\n/g, "").replace(/\s/g, "") != "" ) { // .replace function is used to prevent anomalies in main website due to mere new line(\n) or spaces, which might occasionally go unnoticed
        val = val + "      " + "remarks: \"" + $(this).find("input.remarks_rating").val() + "\"\n";
      }
    });
    return val;
  } else if ($(".infocard-rating input.src_rating_chk[type='checkbox']").length > 1) {
    alert("Checkbox for 'Source for Rating' is unchecked, but there are multiple items listed. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
function src_rating_present() {
  if ($("#rating_chk").is(":checked") && $("input.src_rating_chk[type='checkbox']:checked").length > 0) {
    return true;
  } else {
    return false;
  }
}
// features
function features() {
  const totalRecs = $(".infocard-features tbody tr").length;
  if ($("#features_chk").is(":checked")) {
    //checbox for sys_req is ticked
    val = "features:\n";
    
    $(".infocard-features tbody tr").each(function() {
      val = val +
      "  - " + "name: \"" + $(this).find("input.name_features").val() + "\"\n" +
      "    " + "value: \"" + $(this).find("textarea.value_features").val().replace(/\n/g, "\\n").replace(/\\-/g,"\\\\-") + "\"\n" +
      "    " + "tag: " + "[\"" + $(this).find("input.tag_features").val().replace(/,\s/g, "\", \"") + "\"]\n" +
      "    " + "remarks: \"" + $(this).find("input.remarks_features").val().replace(/\n/g, "\\n").replace(/\\-/g,"\\\\-") + "\"\n";
    });
    
    val = val + "\n";
    return val;
  } else if (totalRecs > 1) {
    alert("Checkbox for 'Features' is unchecked, but there are multiple items listed. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}

function src_features() {
  if ($("#features_chk").is(":checked") && $(".infocard-features input.src_features_chk").is(":checked")) {
    val = "  features: " + "[\"" + $(".infocard-features textarea.src_features").val().replace(/,\s/g, "\", \"") + "\"]\n";
    return val;
  } else if ($(".infocard-features textarea.src_features").val()) {
    alert("Checkbox for 'Source for Features' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
function src_features_present() {
  if ($("#features_chk").is(":checked") && $(".infocard-features input.src_features_chk").is(":checked")) {
    return true;
  } else {
    return false;
  }
}
// system requirements
function sysreq() {
  if ($("#sysreq_chk").is(":checked")) {
    //checbox for sysreq is ticked
    val = "sysreq:\n";
    
    $(".sysreq_version").each(function() {

      if ($(this).find("input.sysreq_version_name").val() !== "") {
   
        val = val + "  - version: \"" + $(this).find("input.sysreq_version_name").val().replace("\"", "\'") + "\"" + "\n" + "    data:\n";
   
        $(this).find(".sysreq_nav").each(function() {
          
          if ($(this).find("input.sysreq_nav_name").val() !== "") {
            val = val + "      - title: \"" + $(this).find("input.sysreq_nav_name").val().replace("\"", "\'") + "\"" + "\n";
            val = val + "        data:\n";

            $(this).find(".sysreq_table_div tbody tr").each(function(i) {
              val = val + "          -\n";
              if ($(this).parents(".sysreq_table_div").find("input.min_sysreq_chk").is(":checked")) {
                //min is ticked
                val = val + "            " + "min: \"" + $(this).find("textarea.min_sysreq").val().replace(/\n/g, "\\n").replace(/\\-/g,"\\\\-") + "\"\n";
              }
              if ($(this).parents(".sysreq_table_div").find("input.recm_sysreq_chk").is(":checked")) {
                //recm is ticked
                val = val + "            " + "recm: \"" + $(this).find("textarea.recm_sysreq").val().replace(/\n/g, "\\n").replace(/\\-/g,"\\\\-") + "\"\n";
              }
              if ($(this).parents(".sysreq_table_div").find("input.optm_sysreq_chk").is(":checked")) {
                //optm is ticked
                val = val + "            " + "optm: \"" + $(this).find("textarea.optm_sysreq").val().replace(/\n/g, "\\n").replace(/\\-/g,"\\\\-") + "\"\n";
              }
            });
          }

        });

      }

    });
    val = val + "\n";
    return val;
  } else if ($(".infocard-sysreq input.sysreq_version_name").length > 1) {
    alert("Checkbox for 'System Requirements' is unchecked, but multiple versions are listed. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
function src_sysreq() {
  if ($("#sysreq_chk").is(":checked") && ($("input.src_sysreq_chk:checked").length > 0)) { 
    val = "  sysreq:\n"; // hugo treats empty string as false in if statement
    
    $(".sysreq_version").each(function() {
      if ($(this).find("input.sysreq_version_name").val() !== "") {
        val = val + "    - version: \"" + $(this).find("input.sysreq_version_name").val().replace("\"", "\'") + "\"\n";
        val = val + "      data:\n";

        $(this).find(".sysreq_nav").each(function() {
          if ($(this).find("input.sysreq_nav_name").val() !== "" && $(this).find(".sysreq_table_div input.src_sysreq_chk").is(":checked")) {
            val = val + "        - title: \"" + $(this).find("input.sysreq_nav_name").val().replace("\"", "\'") + "\"\n";
            val = val + "          data: [\"" + $(this).find("textarea.src_sysreq").val().replace(/,\s/g, "\", \"") + "\"]\n";
          }
        });
      }
    });
    return val;
  } else {
    return "";
  }
}
function src_sysreq_present() {
  let srcSysreqPresent = false;
  if ($("#sysreq_chk").is(":checked")) {
    $(".sysreq_nav").each(function(i) {
      if ($(this).find(".sysreq_table_div input.src_sysreq_chk").is(":checked")) {
        srcSysreqPresent = true;
      }
    });
  }
  return srcSysreqPresent;
}
// genre
function genre() {
  const totalRecs = $(".infocard-genre .genre_rec").length;
  if ($("#genre_chk").is(":checked")) {
    
    val = "genre: [\"";
    
    $(".infocard-genre .genre_rec").each(function(i) {
      val = val + $(this).find("input.genre").val();
      
      if (i === totalRecs -1) {
        val = val + "\"]\n\n";
      } else {
        val = val + "\", \"";
      }
    });
    
    return val;
  } else if (totalRecs > 1) {
    alert("Checkbox for 'Genre' is unchecked, but multiple entries are present. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// developer
function developer() {
  const totalRecs = $(".infocard-developer .developer_rec").length;
  if ($("#developer_chk").is(":checked")) {
    
    val = "developer: [\"";
    
    $(".infocard-developer .developer_rec").each(function(i) {
      val = val + $(this).find("input.developer").val();
      
      if (i === totalRecs -1) {
        val = val + "\"]\n\n";
      } else {
        val = val + "\", \"";
      }
    });
    
    return val;
  } else if (totalRecs > 1) {
    alert("Checkbox for 'Developer' is unchecked, but multiple entries are present. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}

function src_developer() {
  if ($("#developer_chk").is(":checked") && $(".infocard-developer input.src_developer_chk").is(":checked")) {
    val = "  developer: " + "[\"" + $(".infocard-developer textarea.src_developer").val().replace(/,\s/g, "\", \"") + "\"]\n";
    return val;
  } else if ($(".infocard-developer textarea.src_developer").val()) {
    alert("Checkbox for 'Source for Developer' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
function src_developer_present() {
  if ($("#developer_chk").is(":checked") && $(".infocard-developer input.src_developer_chk").is(":checked")) {
    return true;
  } else {
    return false;
  }
}
// license
function license() {
  const totalRecs = $(".infocard-license .license_rec").length;
  if ($("#license_chk").is(":checked")) {
    
    val = "license: [\"";
    
    $(".infocard-license .license_rec").each(function(i) {
      val = val + $(this).find("input.license").val();
      
      if (i === totalRecs -1) {
        val = val + "\"]\n\n";
      } else {
        val = val + "\", \"";
      }
      
    });
    
    return val;
  } else if (totalRecs > 1) {
    alert("Checkbox for 'License' is unchecked, but multiple entries are found. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}

function src_license() {
  if ($("#license_chk").is(":checked") && $(".infocard-license input.src_license_chk").is(":checked")) {
    val = "  license: " + "[\"" + $(".infocard-license textarea.src_license").val().replace(/,\s/g, "\", \"") + "\"]\n";
    return val;
  } else if ($(".infocard-license textarea.src_license").val()) {
    alert("Checkbox for 'Source for License' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}

function src_license_present() {
  if ($("#license_chk").is(":checked") && $(".infocard-license input.src_license_chk").is(":checked")) {
    return true;
  } else {
    return false;
  }
}
// initial release
function initial_release() {
  if ($("#initial_release_chk").is(":checked")) {
    val = "initial_release: " + "[\"" + $(".infocard-initial_release input.initial_release").val().replace(/,\s/g, "\", \"") + "\"]\n\n";
    return val;
  } else if ($(".infocard-initial_release input.initial_release").val()) {
    alert("Checkbox for 'Initial Release' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
function src_initial_release() {
  if ($("#initial_release_chk").is(":checked") && $(".infocard-initial_release input.src_initial_release_chk").is(":checked")) {
    val = "  initial_release: " + "[\"" + $(".infocard-initial_release textarea.src_initial_release").val().replace(/,\s/g, "\", \"") + "\"]\n";
    return val;
  } else if ($(".infocard-initial_release textarea.src_initial_release").val()) {
    alert("Checkbox for 'Sorce for Initial Release' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
function src_initial_release_present() {
  if ($("#initial_release_chk").is(":checked") && $(".infocard-initial_release input.src_initial_release_chk").is(":checked")) {
    return true;
  } else {
    return false;
  }
}
// repository
function repository() {
  if ($("#repository_chk").is(":checked")) {
    val = "repository: " + "[\"" + $(".infocard-repository textarea.repository").val().replace(/,\s/g, "\", \"") + "\"]\n\n";
    return val;
  } else if ($(".infocard-repository textarea.repository").val()) {
    alert("Checkbox for 'Repository' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// written in
function written_in() {
  const totalRecs = $(".infocard-written_in .written_in_rec").length;
  if ($("#written_in_chk").is(":checked")) {
    val = "written_in: [\"";
    
    $(".infocard-written_in .written_in_rec").each(function(i) {
      val = val + $(this).find("input.written_in").val();
      
      if (i === (totalRecs - 1)) {
        val = val + "\"]\n\n";
      } else {
        val = val + "\", \"";
      }
    });
    return val;
  } else if (totalRecs > 1) {
    alert("Checkbox for 'Written in' is unchecked, but multiple entries are listed. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
function src_written_in() {
  if ($("#written_in_chk").is(":checked") && $(".infocard-written_in input.src_written_in_chk").is(":checked")) {
    val = "  written_in: " + "[\"" + $(".infocard-written_in textarea.src_written_in").val().replace(/,\s/g, "\", \"") + "\"]\n";
    return val;
  } else if ($(".infocard-written_in textarea.src_written_in").val()) {
    alert("Checkbox for 'Source for Written in' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
function src_written_in_present() {
  if ($("#written_in_chk").is(":checked") && $(".infocard-written_in input.src_written_in_chk").is(":checked")) {
    return true;
  } else {
    return false;
  }
}
// platform
function platform() {
  if ($("#platform_chk").is(":checked")) {
    //checbox for sys_req is ticked
    val = "platform:\n";
    
    let env_platform_print = function (lower_env, env) {
      // lower_env takes values from $platform_list as string
      if ($("input." + lower_env + "_platform_chk").is(":checked")) {
        val = val + "  - hardware: \"" + env + "\"\n";
        val = val + "    data:\n";

        $(".infocard-platform ." + lower_env + "_platform-rec").each(function(i) {
          if ($("input." + lower_env + "_platform").val() !== "" ) {
            val = val + 
            "      - " + "name: \"" + $(this).find("input." + lower_env + "_platform").val() + "\"\n";
            
            if ($(this).find("input." + lower_env + "_official_platform_rec_chk[type='checkbox']").is(":checked")) {
              val = val +
              "        " + "official: " + "true\n";
            } else {
              val = val +
              "        " + "official: " + "false\n";
            }
            // For arch
            if (lower_env === "desktop") {
              if ($(this).find("input." + lower_env + "_arch32_platform_rec_chk[type='checkbox']").is(":checked") || $(this).find("input." + lower_env + "_arch64_platform[type='checkbox']").is(":checked")) {
                val = val +
                "        " + "arch: [";
                
                if ($(this).find("input." + lower_env + "_arch32_platform_rec_chk[type='checkbox']").is(":checked")) {
                  val = val + "\"x32\"";
                  if ($(this).find("input." + lower_env + "_arch64_platform_rec_chk[type='checkbox']").is(":checked")) {
                    val = val + ", ";
                  }
                }
                if ($(this).find("input." + lower_env + "_arch64_platform_rec_chk[type='checkbox']").is(":checked")) {
                  val = val + "\"x64\"";
                }
                val = val + "]\n";
              }
            } /*else if (lower_env === "browser") {
              if ($(this).find("input." + lower_env + "_extension_platform_rec_chk[type='checkbox']").is(":checked")) {
                val = val + 
                "        " + "extension: true\n";
              }
            }*/
          }
        });
      } else if ($(".infocard-platform input." + lower_env + "_platform_rec_chk[type='checkbox']").length > 1) {
        alert("Checkbox for '" + lower_env + " under Platform' is unchecked, but multiple entries are listed. Please make sure that appropriate checkbox is checked.");
      }
    }
    
    {{ range $platform_list }}
    env_platform_print("{{ lower . }}", "{{ . }}");
    {{ end }}
    
    val = val + "\n";
    return val;
  } else {
    return "";
  }
}
function src_platform() {
  let src_platform_is_present = false;
  if ($("#platform_chk").is(":checked")) {
    $("input.src_platform_chk").each(function() {
      if ($(this).is(":checked")) {
        src_platform_is_present = true;
      }
    })
  }

  if (src_platform_is_present) {

  val = "  platform:\n";

  let platform_chk = function (lower_env, env) {
    if ($("input.src_" + lower_env + "_platform_chk").is(":checked")) {
      val = val + "    - hardware: \"" + env + "\"\n"
      val = val + "      data: [\"" + $("textarea.src_" + lower_env + "_platform").val().replace(/,\s/g, "\", \"") + "\"]\n";
    } else if ($(".infocard-platform textarea.src_" + lower_env + "_platform").val()) {
      alert("Checkbox for 'Source for " + lower_env + " under Platform' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    }
  }

  {{ range $platform_list }}
  platform_chk("{{ lower . }}", "{{ . }}");
  {{ end }}
  
    return val;
  } else {
    return "";
  }
}
function src_platform_present() {
  let src_platform_is_present = false;
  if ($("#platform_chk").is(":checked")) {
    $("input.src_platform_chk").each(function() {
      if ($(this).is(":checked")) {
        src_platform_is_present = true;
      }
    })
    return src_platform_is_present;
  } else {
    return false;
  }
}


// categories
function categories() {
  const totalRecs = $(".infocard-categories .categories_rec").length;
  if ($("#categories_chk").is(":checked")) {
    val = "categories: [\"";
    $(".infocard-categories .categories_rec").each(function(i) {
      val = val + $(this).find("input.categories").val();
      if (i === totalRecs -1) {
        val = val + "\"]\n\n";
      } else {
        val = val + "\", \"";
      }
    });
    return val;
  } else if (totalRecs > 1) {
    alert("Checkbox for 'Categories' is unchecked, but multiple entries are listed. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// aliases
function aliases() {
  const totalRecs = $(".infocard-aliases .aliases_rec").length;
  if ($("#aliases_chk").is(":checked")) {
    val = "aliases: [\"";
    $(".infocard-aliases .aliases_rec").each(function(i) {
      val = val + $(this).find("input.aliases").val();
      if (i === totalRecs -1) {
        val = val + "\"]\n\n";
      } else {
        val = val + "\", \"";
      }
    });
    return val;
  } else if (totalRecs > 1) {
    alert("Checkbox for 'aliases' is unchecked, but multiple entries are listed. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// social
function social() {
  const totalRecs = $(".infocard-social tbody tr").length;
  if ($("#social_chk").is(":checked")) {
    //checbox for sys_req is ticked
    val = "social:\n";
    
    $(".infocard-social tbody tr").each(function() {
      val = val +
      "  - " + "name: \"" + $(this).find("input.name_social").val() + "\"\n" +
      "    " + "url: \"" + $(this).find("textarea.url_social").val() + "\"\n";
            
    });
    
    val = val + "\n";
    return val;
  } else if (totalRecs > 1) {
    alert("Checkbox for 'Social' is unchecked, but multiple entris are listed. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// quick_links
function quick_links() {
  const totalRecs = $(".infocard-quick_links tbody tr").length;
  if ($("#quick_links_chk").is(":checked")) {
    //checbox for sys_req is ticked
    val = "quick_links:\n";
    
    $(".infocard-quick_links tbody tr").each(function() {
      val = val +
      "  - " + "name: \"" + $(this).find("input.name_quick_links").val() + "\"\n" +
      "    " + "url: \"" + $(this).find("textarea.url_quick_links").val() + "\"\n";
            
    });
    
    val = val + "\n";
    return val;
  } else if (totalRecs > 1) {
    alert("Checkbox for 'Quick Links' is unchecked, but multiple entries are listed. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// note
function note() {
  if ($("#note_chk").is(":checked")) {
    val = "note: \"" + $(".infocard-note textarea.note").val().replace(/\n/g, "\\n").replace(/\\-/g,"\\\\-") + "\"\n";
    // .replace function is used to prevent anomalies in main website due to new line(\n)
    val = val + "\n";
    return val;
  } else if ($(".infocard-note textarea.note").val()) {
    alert("Checkbox for 'Note' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked.");
    return "";
  } else {
    return "";
  }
}
// source
function source() {
  if (src_present === true) {
    val = "source:\n" + src_features() + src_developer() + src_written_in() + src_initial_release() + src_license() + src_status() + src_platform() + src_sysreq() + src_rating() + "\n";
    return val;
  } else {
    return "";
  }
}
// save
// Based on https://thiscouldbebetter.wordpress.com/2012/12/18/loading-editing-and-saving-a-text-file-in-html5-using-javascrip/
function saveFormAsTextFile() {
  let textToSave = "---\n" + title() + description() + publishDate() + aliases() + genre() + keywords() + image() + status() + website() + get_it() + quick_links() + features() + platform() + social() + sysreq() +
  rating() + developer() + written_in() +
    initial_release() + repository() + license() + categories() + note() + source() + "---";

  let textToSaveAsBlob = new Blob([textToSave], {
    type: "text/plain"
  });
  let textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);

  let fileNameToSaveAs = "";
  if ($("#filename_chk").is(":checked"))
  {
    fileNameToSaveAs = document.getElementById("filename").value.filenamify() + ".md";
  }
  else if ($(".infocard-filename input.filename").val())
  {
    alert("Checkbox for 'File name' is unchecked, but textbox is not empty. Please make sure that appropriate checkbox is checked. File name and image name has been derived from Title for the file saved.");
    fileNameToSaveAs = document.getElementById("title").value.filenamify() + ".md";
  }
  else
  {
    fileNameToSaveAs = document.getElementById("title").value.filenamify() + ".md";
  }

  let downloadLink = document.createElement("a");
  downloadLink.download = fileNameToSaveAs;
  downloadLink.innerHTML = "Download File";
  downloadLink.href = textToSaveAsURL;
  downloadLink.onclick = destroyClickedElement;
  downloadLink.style.display = "none";
  document.body.appendChild(downloadLink);

  downloadLink.click();
}
function destroyClickedElement(event) {
  document.body.removeChild(event.target);
}

// .page-loading to be removed once window is fully loaded
$(window).on("load", function() {
  $(".page-loading").remove();
  // disable all the inputs on page load
  $("input.sf-chk").each(function () {
    $(this).prop("checked", false);
    $(this).closest(".sf-component").find("input, button, textarea").not(this).not($(this).find(" > input")).each(function () {
      $(this).eq(0).prop("disabled", true);
    });
  });
});


$(function() {
  // Turn navbar solid on scroll
  // Transition effect for navbar
  $(window).scroll(function() {
    // checks if window is scrolled more than 30px, adds/removes bg-white and shadow class
    if ($(this).scrollTop() > 30) {
      $(".navbar").addClass("bg-nav shadow");
    } else {
      $(".navbar").removeClass("bg-nav shadow");
    }
  });
  
  // initialize popovers, used in navbar
  $("[data-toggle='popover']").popover();

  // ui control (add/remove rows), checkbox handling
   
  // image
  $(".infocard-title input.title").on("change", function() {
    image_name = $(this).val().toLowerCase().replace(/\s/g, "-").replace(/\(|\)/g, "");
    $("b.image-name").html(image_name);
  });



/* we will work on generalazing get-it at last...done...almost...the only problem is that if we add rec-chk for platform-get-it, then different logic needs to be used...class names or data attributes representing levels of nested elements can be used...this way we can check like if blah blah and this has class lev-1, then find(labh lahb).hasclass(lev-1) ... this way we can filter and apply logic to only the elements on the same level of nesting as the current (this) element
  // get it
  */
  
  // note: event propogation and event delegation - https://learn.jquery.com/events/event-delegation/
  // https://learningjquery.com/2014/05/jquery-on-not-working-for-dynamically-added-element
  // using .on() as we are dealing with dynamically added elements
  $(".infocard-rating").on("change", "tbody input.users_rating_rdo", function() {
    if (!$(this).is(":checked")) {
      $(this).parents(".form-check").find("input.num_rating").prop("disabled", true);
    } else {
      $(this).parents(".form-check").find("input.num_rating").prop("disabled", false);
    }
  });

  // disable textbox class recm_sysreq when checkbox for id recm_sysreq_chk is uncheck; repeat for min and optm
  $(".infocard-sysreq").on("click", "input.min_sysreq_chk, button.sysreq_add_rec", function() {
    if (!$(this).parents(".sysreq_table_div").find("input.min_sysreq_chk").is(":checked")) {
      $(this).parents(".sysreq_table_div").find("textarea.min_sysreq").prop("disabled", true);
    } else {
      $(this).parents(".sysreq_table_div").find("textarea.min_sysreq").prop("disabled", false);
    }
  });
  $(".infocard-sysreq").on("click", "input.recm_sysreq_chk, button.sysreq_add_rec", function() {
    if (!$(this).parents(".sysreq_table_div").find("input.recm_sysreq_chk").is(":checked")) {
      $(this).parents(".sysreq_table_div").find("textarea.recm_sysreq").prop("disabled", true);
    } else {
      $(this).parents(".sysreq_table_div").find("textarea.recm_sysreq").prop("disabled", false);
    }
  });
  $(".infocard-sysreq").on("click", "input.optm_sysreq_chk, button.sysreq_add_rec", function() {
    if (!$(this).parents(".sysreq_table_div").find("input.optm_sysreq_chk").is(":checked")) {
      $(this).parents(".sysreq_table_div").find("textarea.optm_sysreq").prop("disabled", true);
    } else {
      $(this).parents(".sysreq_table_div").find("textarea.optm_sysreq").prop("disabled", false);
    }
  });
  
    
  const markup_all = {
    aliases: "<div class='form-group aliases_rec sf-rec'> " +
    "<div class='form-check'> " +
    "<label class='form-check-label aliases_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input aliases_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label>" +
    "</div>" +
    "<input type='text' list='aliases_list' class='form-control shadow-sm aliases' id='aliases" + "0" + "' placeholder='old-url'>" +
    "</div>",
    genre: "<div class='form-group genre_rec sf-rec'> " +
    "<div class='form-check'> " +
    "<label class='form-check-label genre_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input genre_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label>" +
    "</div>" +
    "<input type='text' list='genre_list' class='form-control shadow-sm genre' id='genre" + "0" + "' placeholder='genre'>" +
    "</div>",
    developer: "<div class='form-group developer_rec sf-rec'> " +
    "<div class='form-check'> " +
    "<label class='form-check-label developer_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input developer_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label>" +
    "</div>" +
    "<input type='text' list='developer_list' class='form-control shadow-sm developer' id='developer" + "0" + "' placeholder='developer'>" +
    "</div>",
    features: "<tr class='sf-rec'> " +
    "<th scope='row'> <div class='form-check'> " +
    "<label class='form-check-label features_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input features_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label> " +
    "</div></th> " +
    "<td>" +
    "<input type='text' list='features_list' class='form-control shadow-sm name_features' id='name_features" + "0" + "' placeholder='Name'> " +
    "<input type='text' class='form-control shadow-sm tag_features' id='tag_features" + "0" + "' placeholder='Tag1, Tag2, Tag3, ...'> " +
    "<input type='text' class='form-control shadow-sm remarks_features' id='remarks_features" + "0" + "' placeholder='Remarks'> " +
    "</td>" +
    "<td>" +
    "<textarea class='form-control shadow-sm value_features' id='value_features" + "0" + "' cols='30' rows='2' placeholder='Value (markdown supported!)'></textarea> " +
    "</td>" +
    "</tr>",
    quick_links: "<tr class='sf-rec'> " +
    "<th scope='row'> <div class='form-check'> " +
    "<label class='form-check-label quick_links_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input quick_links_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label> " +
    "</div></div></th> " +
    "<td> " +
    "<input type='text' list='quick_links_list' class='form-control shadow-sm name_quick_links' id='name_quick_links" + "0" + "' placeholder='Name'> " +
    "</td>" +
    "<td> " +
    "<textarea class='form-control shadow-sm url_quick_links' id='url_quick_links" + "0" + "' cols='30' rows='1' placeholder='URL'></textarea> " +
    "</td>" +
    "</tr>",
    get_it: "<tr class='sf-rec'> " +
    "<th scope='row'> <div class='form-check'> " +
    "<label class='form-check-label get_it_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input get_it_rec_chk sf-rec-chk' name='" + "0" + "'> " +
    "<span class='sf-label-text'>0</span>" + "</label> " +
    "</div></th> " +
    "<td> " +
    "<input type='text' list='get_it_list' class='form-control shadow-sm name_get_it' id='name_get_it" + "0" + "' placeholder='Name'> " +
    "<label class='form-check-label authentic_get_it'>" +
      "<input type='checkbox' class='authentic_get_it' checked=''>" +
      "<span class='ml-2'>Authentic</span>" +
    "</label>" +
    "</td>" +
    "<td class='sf-component'> " +
    "<div class='platform_get_it_rec sf-body'>" +
      "<div class='form-group'>" +
        "<input type='text' list='platform_get_it_list' class='form-control platform_name_get_it' id='platform_name_get_it" + "0" + "_1' placeholder='Platform Name'>" +
        "<select name='device' class='select my-1 hardware_get_it_list' id='hardware_get_it_list" + "0" + "_1'>" +
        "<option selected>Device...</option>" +
        {{ range $hardware_get_it_list }}
        "<option value='{{ . }}'>{{ . }}</option>" +
        {{ end }}
        "</select>" +
        "<br>" +
        "<div class='form-check form-check-inline'>" +
        "<label class='form-check-label platform_arch32_get_it_chk'>" +
        "<input type='checkbox' class='form-check-input platform_arch32_get_it_chk'>" +
        " x32</label>" +
        "</div>" +
        "<div class='form-check form-check-inline'>" +
        "<label class='form-check-label platform_arch64_get_it_chk'>" +
        "<input type='checkbox' class='form-check-input platform_arch64_get_it_chk'>" +
        " x64</label>" +
        "</div>" +
        "<div class='form-check'>" +
        "<label class='form-check-label platform_type_get_it_chk'>" +
        "<input type='checkbox' class='form-check-input platform_type_get_it_chk' checked=''>" +
          " Official</label>" +
        "</div>" +        
        "<input type='text' class='form-control shadow-sm platform_remarks_get_it' size='8' placeholder='Remarks'>" +
      "</div>" +
      "</div>" +
      "<button class='btn btn-light platform_get_it_add_rec sf-add-rec' type='button' data-identity='platform_get_it' name='" + "0" + "' id='platform_get_it_add_rec" + "0" + "'>+</button>" +
    "</td>" +
    "<td> " +
    "<textarea class='form-control shadow-sm url_get_it' id='url_get_it" + "0" + "' cols='30' rows='1' placeholder='URL'></textarea> " +
    "</td>" +
    "</tr>",
    platform_get_it: "<div class='form-group'>" +
    "<hr> <input type='text' list='platform_get_it_list' class='form-control platform_name_get_it' id='platform_name_get_it" + "0" + "_" + "0" + "' placeholder='Platform Name'>" +
    "<select name='device' class='select my-1 hardware_get_it_list' id='hardware_get_it_list" + "0" + "_" + "0" + "'>" +
      "<option selected>Device...</option>" +
      {{ range $hardware_get_it_list }}
      "<option value='{{ . }}'>{{ . }}</option>" +
      {{ end }}
    "</select>" +
    "<br>" +
    "<div class='form-check form-check-inline'>" +
    "<label class='form-check-label platform_arch32_get_it_chk'>" +
    "<input type='checkbox' class='form-check-input platform_arch32_get_it_chk'>" +
    " x32</label>" +
    "</div>" +
    "<div class='form-check form-check-inline'>" +
    "<label class='form-check-label platform_arch64_get_it_chk'>" +
    "<input type='checkbox' class='form-check-input platform_arch64_get_it_chk'>" +
    " x64</label>" +
    "</div>" +
    "<div class='form-check'>" +
    "<label class='form-check-label platform_type_get_it_chk'>" +
    "<input type='checkbox' class='form-check-input platform_type_get_it_chk' checked=''>" +
    " Official</label>" +
    "</div>" +
    "<input type='text' class='form-control shadow-sm platform_remarks_get_it' size='8' placeholder='Remarks'>" +
    "</div>",
    {{ range $platform_list }}
    {{ replace (lower .) "-" "_" }}_platform: "<div class='form-group " + "{{ lower . }}" + "_platform-rec sf-rec'> <div class='form-check'> " +
    "<label class='form-check-label " + "{{ lower . }}" + "_platform_rec_chk sf-rec-chk'> " +  
    "<input type='checkbox' class='form-check-input " + "{{ lower . }}" + "_platform_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label> " +
    "</div>" +
    "<input type='text' list='" + "{{ lower . }}" + "_platform_list' class='form-control shadow-sm " + "{{ lower . }}" + "_platform' id='" + "{{ lower . }}" + "_platform" + "0" + "' placeholder='Name'> " +
    "<div class='form-check'> " +
    "<label class='form-check-label " + "{{ lower . }}" + "_official_platform_rec_chk'>" +
    "<input type='checkbox' class='form-check-input " + "{{ lower . }}" + "_official_platform_rec_chk' checked=''> " +
    " Official</label> " +
    "</div>" +
    {{ if eq (lower .) "desktop" }}
    "<div class='form-check form-check-inline'> " +
    "<label class='form-check-label " + "{{ lower . }}" + "_arch32_platform_rec_chk'>" +
    "<input type='checkbox' class='form-check-input " + "{{ lower . }}" + "_arch32_platform_rec_chk'> " +
    " x32</label> " +
    "</div> " +
    "<div class='form-check form-check-inline'> " +
    "<label class='form-check-label " + "{{ lower . }}" + "_arch64_platform_rec_chk'>" +
    "<input type='checkbox' class='form-check-input " + "{{ lower . }}" + "_arch64_platform_rec_chk'> " +
    " x64</label> " +
    "</div>" +
    {{/* else if eq (lower .) "browser" }}
    "<div class='form-check form-check-inline'>" +
    "<label class='form-check-label {{ lower . }}_extension_platform_rec_chk'>" +
    "<input type='checkbox' class='form-check-input {{ lower . }}_extension_platform_rec_chk'>" +
    "Extension/Add-on" +
    "</label>" +
    "</div>" +  */}}
    {{ end }}
    "</div>",
    {{ end }}
    sysreq_nav: "<tr class='sf-rec'> " +
    "<th scope='row'> <div class='form-check'> " +
    "<label class='form-check-label sysreq_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input sysreq_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label> " +
    "</div></th> " +
    "<td> " +
    "<textarea class='form-control shadow-sm min_sysreq' cols='30' rows='2' placeholder='Minimum RAM/CPU/GPU...'></textarea> " +
    "</td> " +
    "<td> " +
    "<textarea class='form-control shadow-sm recm_sysreq' cols='30' rows='2' placeholder='Recommended RAM/CPU/GPU...'></textarea> " +
    "</td> " +
    "<td> " +
    "<textarea class='form-control shadow-sm optm_sysreq' cols='30' rows='2' placeholder='Optimal RAM/CPU/GPU...'></textarea> " +
    "</td>" +
    "</tr>",
    sysreq_version: "<hr>" +
    "<div class='sysreq_nav sf-rec'>" +
      "<input type='text' class='form-control sysreq_nav_name' placeholder='Nav name'>" +
      "<div class='sysreq_table_div sf-component'>" +
        "<div class='table-responsive-md'>" +
          "<table class='table table-bordered'>" +
            "<thead>" +
              "<tr>" +
                "<th scope='col'>" +
                  "#" +
                "</th>" +
                "<th scope='col'>" +
                  "<div class='form-check'>" +
                  "<label class='form-check-label'>" +
                  "<input type='checkbox' class='form-check-input min_sysreq_chk'>" +
                  " Minimum</label>" +
                  "</div>" +
                "</th>" +
                "<th scope='col'>" +
                  "<div class='form-check'>" +
                  "<label class='form-check-label'>" +
                  "<input type='checkbox' class='form-check-input recm_sysreq_chk'>" +
                  " Recommended</label>" +
                  "</div>" +
                "</th>" +
                "<th scope='col'>" +
                  "<div class='form-check'>" +
                  "<label class='form-check-label'>" +
                  "<input type='checkbox' class='form-check-input optm_sysreq_chk'>" +
                  " Optimal</label>" +
                  "</div>" +
                "</th>" +
              "</tr>" +
            "</thead>" +
            "<tbody class='sf-body'>" +
              "<tr class='sf-rec'>" +
                "<th scope='row'>" +
                  "<div class='form-check'>" +
                  "<label class='form-check-label sysreq_rec_chk sf-rec-chk'>" +
                  "<input type='checkbox' class='form-check-input sysreq_rec_chk sf-rec-chk'>" +
                  "<span class='sf-label-text'>1</span></label>" +
                  "</div>" +
                "</th>" +
                "<td>" +
                    "<textarea class='form-control shadow-sm min_sysreq' cols='30' rows='2' placeholder='Minimum RAM/CPU/GPU...'></textarea>" +
                "</td>" +
                "<td>" +
                    "<textarea class='form-control shadow-sm recm_sysreq' cols='30' rows='2' placeholder='Recommended RAM/CPU/GPU...' disabled></textarea>" +
                "</td>" +
                "<td>" +
                    "<textarea class='form-control shadow-sm optm_sysreq' cols='30' rows='2' placeholder='Optimal RAM/CPU/GPU...' disabled></textarea>" +
                "</td>" +
              "</tr>" +
            "</tbody>" +
          "</table>" +
        "</div>" +
        "<button class='btn btn-light sysreq_add_rec sf-add-rec' data-identity='sysreq_nav' type='button'>Add Row</button>" +
        "<button class='btn btn-danger sysreq_delete_rec sf-delete-rec' type='button'>Delete Selected Row</button>" +
        "<br><br>" +
        "<div class='form group sf-component'>" +
          "<div class='form-check'>" +
            "<label class='form-check-label sf-chk'> <input class='form-check-input src_sysreq_chk sf-chk' type='checkbox' checked=''>  Source:</label>" +
          "</div>" +
          "<label class='d-block small src_sysreq'>" +
          "<textarea class='form-control shadow-sm src_sysreq' cols='30' rows='1' placeholder='Source1, Source2, ...'></textarea>" +
          "mind the space after comma while separating two sources :)</label>" +
        "</div>" +
      "</div>" +
    "</div>",
    sysreq: "<div class='border rounded p-1 my-2 sf-rec'>" + "<details class='sysreq_version sf-component'>" +
    "<summary class='d-flex'><input type='text' class='form-control sysreq_version_name w-75' placeholder='Version name' value='latest'></input> <span class='ml-auto mr-5 my-auto'>🡇</span></summary>" +
    "<div class='sf-body'>" +
    "<div class='sysreq_nav sf-rec'>" +
      "<input type='text' class='form-control sysreq_nav_name' placeholder='Nav name' value='general'>" +
      "<div class='sysreq_table_div sf-component'>" +
        "<div class='table-responsive-md'>" +
          "<table class='table table-bordered'>" +
            "<thead>" +
              "<tr>" +
                "<th scope='col'>" +
                  "#" +
                "</th>" +
                "<th scope='col'>" +
                  "<div class='form-check'>" +
                  "<label class='form-check-label'>" +
                  "<input type='checkbox' class='form-check-input min_sysreq_chk'>" +
                  "Minimum</label>" +
                  "</div>" +
                "</th>" +
                "<th scope='col'>" +
                  "<div class='form-check'>" +
                  "<label class='form-check-label'>" +
                  "<input type='checkbox' class='form-check-input recm_sysreq_chk'>" +
                  " Recommended</label>" +
                  "</div>" +
                "</th>" +
                "<th scope='col'>" +
                  "<div class='form-check'>" +
                  "<label class='form-check-label'>" +
                  "<input type='checkbox' class='form-check-input optm_sysreq_chk'>" +
                    " Optimal</label>" +
                  "</div>" +
                "</th>" +
              "</tr>" +
            "</thead>" +
            "<tbody class='sf-body'>" +
              "<tr class='sf-rec'>" +
                "<th scope='row'>" +
                  "<div class='form-check'>" +
                  "<label class='form-check-label sysreq_rec_chk sf-rec-chk'>" +
                  "<input type='checkbox' class='form-check-input sysreq_rec_chk sf-rec-chk'>" +
                  "<span class='sf-label-text'>1</span></label>" +
                  "</div>" +
                "</th>" +
                "<td>" +
                  "<textarea class='form-control shadow-sm min_sysreq' cols='30' rows='2' placeholder='Minimum RAM/CPU/GPU...'></textarea>" +
                "</td>" +
                "<td>" +
                  "<textarea class='form-control shadow-sm recm_sysreq' cols='30' rows='2' placeholder='Recommended RAM/CPU/GPU...' disabled></textarea>" +
                "</td>" +
                "<td>" +
                  "<textarea class='form-control shadow-sm optm_sysreq' cols='30' rows='2' placeholder='Optimal RAM/CPU/GPU...' disabled></textarea>" +
                "</td>" +
              "</tr>" +
            "</tbody>" +
          "</table>" +
          "<small>markdown supported, except tables.</small>" +
        "</div>" +
        "<button class='btn btn-light sysreq_add_rec sf-add-rec' data-identity='sysreq_nav' type='button'>Add Row</button>" +
        "<button class='btn btn-danger sysreq_delete_rec sf-delete-rec' type='button'>Delete Selected Row</button>" +
        "<br><br>" +
        "<div class='form group sf-component'>" +
          "<div class='form-check'>" +
            "<label class='form-check-label src_sysreq_chk sf-chk'> <input class='form-check-input src_sysreq_chk sf-chk' type='checkbox' checked=''> Source:</label>" +
          "</div>" +
          "<label class='d-block small src_sysreq'>" +
          "<textarea class='form-control shadow-sm src_sysreq' cols='30' rows='1' placeholder='Source1, Source2, ...'></textarea>" +
          "mind the space after comma while separating two sources :)</label>" +
        "</div>" +
      "</div>" +
    "</div>" +
    "</div>" +
    "<button class='btn btn-primary sysreq_add_nav sf-add-rec' data-identity='sysreq_version' data-injection='false' type='button'>Add Nav</button>" +
    "</details></div>",
    rating: "<tr class='sf-rec'> " +
    "<th scope='row'> <div class='form-check'> " +
    "<label class='form-check-label rating_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input rating_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label> " +
    "</div></th> " +
    "<td> <div class='form-group'> " +
    "<input type='text' list='rating_list' class='form-control shadow-sm name_rating' id='name_rating" + "0" + "' size='8' placeholder='Name'> " +
    "<input type='text' class='form-control shadow-sm remarks_rating' id='remarks_rating" + "0" + "' size='8' placeholder='Remarks'> " +
    "</div></td>" +
    "<td> <div class='form-group form-inline'> " +
    "<input type='text' id='rate_rating" + "0" + "' size='2' class='form-control shadow-sm rate_rating' placeholder='Rating'> " +
    "&nbsp;/&nbsp; " +
    "<input type='text' id='outof_rating" + "0" + "' size='2' class='form-control shadow-sm outof_rating' placeholder='Out of'> " +
    "</div></td>" +
    "<td> <div class='form-group'> " +
    "<div class='form-check'> <label class='form-check-label form-inline'> " +
    "<input type='radio' class='form-check-input users_rating_rdo' name='users_pro_rating" + "0" + "' id='users_rating_rdo" + "0" + "' checked=''> " +
    "Based on &nbsp;" +
    "<input type='text' id='num_rating" + "0" + "' size='4' class='form-control shadow-sm num_rating' placeholder='number of'>&nbsp; " +
    "users' opinion " +
    "</label> </div>" +
    "<div class='form-check'> <label class='form-check-label form-inline'> " +
    "<input type='radio' class='form-check-input pro_rating_rdo' name='users_pro_rating" + "0" + "' id='pro_rating_rdo" + "0" + "'> " +
    "Based on a professional's opinion " +
    "</label> </div>" +
    "</div></td>" +
    "<td> <div class='form-group form-inline sf-component'> " +
    "<div class='form-check'> " +
    "<input type='checkbox' class='form-check-input src_rating_chk sf-chk' id='src_rating_chk" + "0" + "' checked=''>" +
    "</div>" +
    "<textarea class='form-control shadow-sm src_rating' id='src_rating" + "0" + "' cols='20' rows='2' placeholder='Source'></textarea> " +
    "</div></td>" +
    "</tr>",
    social: "<tr class='sf-rec'> " +
    "<th scope='row'> <div class='form-check'> " +
    "<label class='form-check-label social_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input social_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label> " +
    "</div></th> " +
    "<td> " +
    "<input type='text' list='social_list' class='form-control shadow-sm name_social' id='name_social" + "0" + "' placeholder='Name'> " +
    "</td>" +
    "<td> " +
    "<textarea class='form-control shadow-sm url_social' id='url_social" + "0" + "' cols='30' rows='1' placeholder='URL'></textarea> " +
    "</td>" +
    "</tr>",
    license: "<div class='form-group license_rec sf-rec'> " +
    "<div class='form-check'> " +
    "<label class='form-check-label license_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input license_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label>" +
    "</div>" +
    "<input type='text' list='license_list' class='form-control shadow-sm license' id='license" + "0" + "' placeholder='License'>" +
    "</div>",
    written_in: "<div class='form-group written_in_rec sf-rec'> " +
    "<div class='form-check'> " +
    "<label class='form-check-label written_in_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input written_in_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label>" +
    "</div>" +
    "<input type='text' list='written_in_list' class='form-control shadow-sm written_in' id='written_in" + "0" + "' placeholder='Computer Language'>" +
    "</div>",
    categories: "<div class='form-group categories_rec sf-rec'> " +
    "<div class='form-check'> " +
    "<label class='form-check-label categories_rec_chk sf-rec-chk'> " +
    "<input type='checkbox' class='form-check-input categories_rec_chk sf-rec-chk'> " +
    "<span class='sf-label-text'>0</span>" + "</label>" +
    "</div>" +
    "<input type='text' list='categories_list' class='form-control shadow-sm categories' id='categories" + "0" + "' placeholder='Category'>" +
    "</div>"

  }


  /*

  sf-infocard: the most parent class for the infocard.
  sf-chk: the class for checkbox(input) and corresponding label. primarily used to enable/disable the elements of infocard. the class can be used for checkbox/label for infocard and also for source input.
  sf-body: the class that contains all the records/rows.
  sf-rec: the most parent class for a records/row.
  sf-rec-chk: the class for checkbox(input) and corresponding label, scoped to a record/row. primarily used for sf-add-rec and sf-delete-rec to function.
  sf-add-rec: the class that adds the records upon clicking it. this element sits outside the sf-body class.
  sf-delete-rec: the class that removes the selected records.

  */
  $(".sf-infocard").on("click", "button.sf-add-rec", function () {
    const infocardName = $(this).attr("data-identity");
    // set data-injection to false if you don't want the ability to add records before selected row-checkboxes
    // note that setting injection to false also disables the serial renumbering of checkboxes and its labels
    const injection = $(this).attr("data-injection");
    
    if (($(this).closest(".sf-component").find(".sf-body:first > .sf-rec input.sf-rec-chk[type=checkbox]:checked").length < 1) || (injection === "false")) {

      $(this).closest(".sf-component").find(".sf-body:first").append(markup_all[infocardName]);

      // below code assigns ids to textarea element in serial manner            
      if (injection !== "false") {
        $(this).closest(".sf-component").find(".sf-body:first > .sf-rec").each(function (i) {
          $(this).find("label.sf-rec-chk span.sf-label-text").eq(0).html(" " + (i + 1));
          // reassign name attribute to radio buttons from rating infocard to group them correctly
          if (infocardName === "rating") {
            let col3 = $(this).find("td")[2];
            $(col3).find("input.users_rating_rdo").eq(0).attr("name","users_pro_rating" + (i + 1));
            $(col3).find("input.pro_rating_rdo").eq(0).attr("name","users_pro_rating" + (i + 1));
          }
            //$(this).find("label.sf-rec-chk").eq(0).attr("for", infocardName + "_rec_chk_" + (i + 1));
          //$(this).find("input.sf-rec-chk").eq(0).attr("id", infocardName + "_rec_chk_" + (i + 1));
        });
      }
      

    } else {

      $(this).closest(".sf-component").find(".sf-body:first > .sf-rec input.sf-rec-chk[type=checkbox]:checked").each(function () {
        $(markup_all[infocardName]).insertBefore($(this).closest(".sf-rec").eq(0));
      });

      // below code assigns ids to textarea element in serial manner            
      $(this).closest(".sf-component").find(".sf-body:first > .sf-rec").each(function (i) {
        $(this).find("label.sf-rec-chk span.sf-label-text").eq(0).html(" " + (i + 1));
        // reassign name attribute to radio buttons from rating infocard to group them correctly
        if (infocardName === "rating") {
          let col3 = $(this).find("td")[2];
          $(col3).find("input.users_rating_rdo").eq(0).attr("name","users_pro_rating" + (i + 1));
          $(col3).find("input.pro_rating_rdo").eq(0).attr("name","users_pro_rating" + (i + 1));
        }
        //$(this).find("label.sf-rec-chk").eq(0).attr("for", infocardName + "_rec_chk_" + (i + 1));
        //$(this).find("input.sf-rec-chk").eq(0).attr("id", infocardName + "_rec_chk_" + (i + 1));
      });
    }
  });

  // Find and remove selected rows
  $(".sf-infocard").on("click", "button.sf-delete-rec", function () {
    const infocardName = $(this).attr("data-identity");
    $(this).closest(".sf-component").find(".sf-body:first > .sf-rec input.sf-rec-chk").each(function () {

      if ($(this).is(":checked")) {
        $(this).closest(".sf-rec").remove();
      }

    });

    // below code corrects numbering and ids in serial manner            
    $(this).closest(".sf-component").find(".sf-body:first > .sf-rec").each(function (i) {
      $(this).find("label.sf-rec-chk span.sf-label-text").eq(0).html(" " + (i + 1));
      // reassign name attribute to radio buttons from rating infocard to group them correctly
      if (infocardName === "rating") {
        let col3 = $(this).find("td")[2];
        $(col3).find("input.users_rating_rdo").eq(0).attr("name","users_pro_rating" + (i + 1));
        $(col3).find("input.pro_rating_rdo").eq(0).attr("name","users_pro_rating" + (i + 1));
      }
      //$(this).find("label.sf-rec-chk").eq(0).attr("for", infocardName + "_rec_chk_" + (i + 1));
      //$(this).find("input.sf-rec-chk").eq(0).attr("id", infocardName + "_rec_chk_" + (i + 1));
    });
  });

  // disable all elements when sf-chk (whether label or input(checkbox)) is disabled
  $(".sf-infocard").on("click", "input.sf-chk, label.sf-chk", function () {
    if ($(this).closest("div").find("input.sf-chk").is(":not(:checked)")) {
      $(this).closest(".sf-component").find("input, button, textarea").not(this).not($(this).find(" > input")).each(function () {
        $(this).eq(0).prop("disabled", true);
      });
    } else {
      $(this).closest(".sf-component").find("input, button, textarea").prop("disabled", false);
    }
  });


  // index.html layout

  // reading a markdown file which has front matter as per the prescibed format (archetypes)
  // https://stackoverflow.com/a/4459419
  // for understanding : http://qnimate.com/an-introduction-to-javascript-blobs-and-file-interface/ 
  function readURL(input) {
    if (input.files && input.files[0]) {
      let reader = new FileReader();
      reader.onload = function(e) {
        // Get document, or throw exception on error
        try {
          // from http://youmightnotneedjquery.com/
          let request = new XMLHttpRequest();
          request.open("GET", e.target.result, true);
          request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
              // Success!
              // resp now contains the text of read file
              let resp = request.responseText;
              
              // having two '---' gives yaml syntax error. hence we replace the one '---' present at last of the file with 'content: |', and then read it.
              // https://stackoverflow.com/a/5497333
              let pos = resp.lastIndexOf("---");
              resp = resp.substring(0, pos) + "content: |" + resp.substring(pos + 3);
              /*
              // https://stackoverflow.com/a/5497365 and https://stackoverflow.com/a/3829530
              let gad = resp.replace(/---([^---]+)$/, "content: |" + "$1");
              // for some reason didn't work for audacity.md and darktable.md
              */
              
              // load the file as JS Object into 'let page' using 'jsyaml library'
              let page = jsyaml.load(resp);
              
              // from here object "page" can be accessed //
              // uncheck all checkboxes, clear all text inputs and textareas
              $("body input[type='checkbox']").prop("checked", false);
              $("body input[type='text'], body input[type='url'], body textarea").val("");
              $("body textarea").html("");
              

              $(document).attr("title", page.title + " | SAW");

              $(".infocard-title input.title").val(page.title);

              if (page.description) {
                $("#description_chk").prop("checked", true);
                $("#description_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);
                $(".infocard-description textarea.description").val(page.description);
              } else {
                $("#description_chk").prop("checked", false);
              }
              if (page.publishDate) {
                $("#publishDate_chk").prop("checked", true);
                $("#publishDate_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-publishDate textarea.publishDate").val(page.publishDate);
              } else {
                $("#publishDate_chk").prop("checked", false);
              }
              if (page.keywords) {
                $("#keywords_chk").prop("checked", true);
                $("#keywords_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-keywords textarea.keywords").val(page.keywords.join(", "));
              } else {
                $("#keywords_chk").prop("checked", false);
              }
              if (page.image) {
                $("#image_chk").prop("checked", true);
                $("#image_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $("input.custom_image_rdo").prop("checked", true);
                $(".infocard-image input.image").val(page.image);
              } else {
                $("#image_chk").prop("checked", false);
              }
              if (page.status) {
                $("#status_chk").prop("checked", true);
                $("#status_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-status select.status").val(page.status);
              } else {
                $("#status_chk").prop("checked", false);
              }
              if (page.website) {
                $("#website_chk").prop("checked", true);
                $("#website_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-website input.website").val(page.website);
              } else {
                $("#website_chk").prop("checked", false);
              }
              if (page.get_it) {
                $("#get_it_chk").prop("checked", true);
                $("#get_it_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                // https://robertnyman.com/2008/04/11/javascript-loop-performance/
                for(let i = 0, len = page.get_it.length-1; i <= len; i++) {
                  if ( i > 0 ) {
                    $(".infocard-get_it button.get_it_add_rec").trigger("click");
                  }
                  // name
                  $(".infocard-get_it tbody input.name_get_it").last().val(page.get_it[i].from);

                  $(".infocard-get_it tbody input.authentic_get_it").last().prop("checked", false)
                  // https://stackoverflow.com/a/1789952/10284939
                  if (page.get_it[i].from.indexOf("Authentic") !== -1) {
                    $(".infocard-get_it tbody input.authentic_get_it").last().prop("disabled", true);;
                  }
                  
                  // platform
                  if (page.get_it[i].platform) {
                    for(let j = 0, len_j = page.get_it[i].platform.length-1; j <= len_j; j++) {
                      if ( j > 0 ) {
                        $(".infocard-get_it tbody button.platform_get_it_add_rec").last().trigger("click");
                      }
                      // platform name
                      $(".infocard-get_it tbody input.platform_name_get_it").last().val(page.get_it[i].platform[j].name);
                      
                      // platform hardware - desktop/laptop or mobile/tab
                      if (page.get_it[i].platform[j].hardware) {
                        let temp_hardware_array = page.get_it[i].platform[j].hardware;
                        $(".infocard-get_it tbody select.hardware_get_it_list").last().val(temp_hardware_array);
                      }
                      // platform architecture - x32/x64
                      if (page.get_it[i].platform[j].arch) {
                        let temp_arch_array = page.get_it[i].platform[j].arch;
                        if (temp_arch_array.includes("x32")) {
                          $(".infocard-get_it tbody input.platform_arch32_get_it_chk").last().prop("checked", true);
                        }
                        if (temp_arch_array.includes("x64")) {
                          $(".infocard-get_it tbody input.platform_arch64_get_it_chk").last().prop("checked", true);
                        }
                      }
                      // platform type - official/unofficial
                      if (page.get_it[i].platform[j].official) {
                        $(".infocard-get_it tbody input.platform_type_get_it_chk").last().prop("checked", true);
                      } else {
                        $(".infocard-get_it tbody input.platform_type_get_it_chk").last().prop("checked", false);
                      }
                      // platform remarks
                      let platform_remarks_get_it = page.get_it[i].platform[j].remarks;
                      if (platform_remarks_get_it) {
                        $(".infocard-get_it tbody input.platform_remarks_get_it").last().val(platform_remarks_get_it);
                      } else {
                        $(".infocard-get_it tbody input.platform_remarks_get_it").last().val("");
                      }
                    }
                  }
                  // url
                  $(".infocard-get_it tbody textarea.url_get_it").last().val(page.get_it[i].url);
                  
                }
              } else {
                $("#get_it_chk").prop("checked", false);
              }
              
              if (page.features) {
                $("#features_chk").prop("checked", true);
                $("#features_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                for(let i = 0, len = page.features.length-1; i <= len; i++) {
                  if ( i > 0 ) {
                    $(".infocard-features button.features_add_rec").trigger("click");
                  }
                  $(".infocard-features input.name_features").last().val(page.features[i].name);
                  $(".infocard-features textarea.value_features").last().val(page.features[i].value);
                  $(".infocard-features input.tag_features").last().val(page.features[i].tag.join(", "));
                  $(".infocard-features input.remarks_features").last().val(page.features[i].remarks);
                }
              } else {
                $("#features_chk").prop("checked", false);
              }

              if (page.content) {
                $("#content_chk").prop("checked", true);
                $("#content_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-content textarea.content").val(page.content);
              } else {
                $("#content_chk").prop("checked", false);
              }
              
              if (page.sysreq) {
                $("#sysreq_chk").prop("checked", true);
                $("#sysreq_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                
                for(let i = 0, len = Object.keys(page.sysreq).length-1; i <= len; i++) {
                  
                  if ( i > 0 ) {
                    $("button.sysreq_add_version").trigger("click");
                  }
                  $(".infocard-sysreq input.sysreq_version_name").last().val((page.sysreq)[i].version);
                  for (let j = 0, platform_len = Object.keys((page.sysreq)[i].data).length - 1; j <= platform_len; j++) {
                  
                    if ( j > 0 ) {
                      $("button.sysreq_add_nav").last().trigger("click");
                    }
                    $(".infocard-sysreq input.sysreq_nav_name").last().val((page.sysreq)[i].data[j].title);
                    
                    for (let k = 0, row_len = Object.keys((page.sysreq)[i].data[j].data).length - 1; k <= row_len; k++) {
                      if ( k > 0 ) {
                      $("button.sysreq_add_rec").last().trigger("click");
                      }
                      
                      if ((page.sysreq)[i].data[j].data[k].min) {
                        $("input.min_sysreq_chk").last().prop("checked", true);
                        $("textarea.min_sysreq").last().val((page.sysreq)[i].data[j].data[k].min);
                      }/* else {    // gets problematic if only last field is empty
                        $("input.min_sysreq_chk").last().prop("checked", false);
                      }*/
                      
                      if ((page.sysreq)[i].data[j].data[k].recm) {
                        $("input.recm_sysreq_chk").last().prop("checked", true);
                        $("textarea.recm_sysreq").last().val((page.sysreq)[i].data[j].data[k].recm);
                      }/* else {
                        $("input.recm_sysreq_chk").last().prop("checked", false);
                      }*/
                      
                      if ((page.sysreq)[i].data[j].data[k].optm) {
                        $("input.optm_sysreq_chk").last().prop("checked", true);
                        $("textarea.optm_sysreq").last().val((page.sysreq)[i].data[j].data[k].optm);
                      }/* else {
                        $("input.optm_sysreq_chk").last().prop("checked", false);
                      }*/
                      
                    }
                  }
                }
              } else {
                $("#sysreq_chk").prop("checked", false);
              }

              if (page.genre) {
                $("#genre_chk").prop("checked", true);
                $("#genre_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                for(let i = 0, len = page.genre.length-1; i <= len; i++) {
                  if ( i > 0 ) {
                    $(".infocard-genre button.genre_add_rec").trigger("click");
                  }
                  $(".infocard-genre input.genre").last().val(page.genre[i]);
                }
              } else {
                $("#genre_chk").prop("checked", false);
              }

              if (page.developer) {
                $("#developer_chk").prop("checked", true);
                $("#developer_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                for(let i = 0, len = page.developer.length-1; i <= len; i++) {
                  if ( i > 0 ) {
                    $(".infocard-developer button.developer_add_rec").trigger("click");
                  }
                  $(".infocard-developer input.developer").last().val(page.developer[i]);
                }
              } else {
                $("#developer_chk").prop("checked", false);
              }

              if (page.initial_release) {
                $("#initial_release_chk").prop("checked", true);
                $("#initial_release_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-initial_release input.initial_release").val(page.initial_release.join(", "));
              } else {
                $("#initial_release_chk").prop("checked", false);
              }
              if (page.repository) {
                $("#repository_chk").prop("checked", true);
                $("#repository_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-repository textarea.repository").val(page.repository.join(", "));
              } else {
                $("#repository_chk").prop("checked", false);
              }
              if (page.written_in) {
                $("#written_in_chk").prop("checked", true);
                $("#written_in_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                for(let i = 0, len = page.written_in.length-1; i <= len; i++) {
                  if ( i > 0 ) {
                    $(".infocard-written_in button.written_in_add_rec").trigger("click");
                  }
                  $(".infocard-written_in input.written_in").last().val(page.written_in[i]);
                }
              } else {
                $("#written_in_chk").prop("checked", false);
              }
              if (page.platform) {
                $("#platform_chk").prop("checked", true);
                $("#platform_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);
                
                let platform_read_func = function (i = 0, lower_env= "desktop", env = "Desktop") {
                  if (page.platform[i].hardware === env) {
                    $("input." + lower_env + "_platform_chk").prop("checked", true);
                    for(let j = 0, env_len = Object.keys(page.platform[i].data).length-1; j <= env_len; j++) {
                      if ( j > 0 ) {
                        $("button." + lower_env + "_platform_add_rec").trigger("click");
                      }
                      $("input." + lower_env + "_platform").last().val(page.platform[i].data[j].name);
                      // official
                      if (page.platform[i].data[j].official) {
                        $("input." + lower_env + "_official_platform_rec_chk[type='checkbox']").last().prop("checked", true);
                      } else {
                        $("input." + lower_env + "_official_platform_rec_chk[type='checkbox']").last().prop("checked", false);
                      }
                      if (lower_env === "desktop") {
                        // Architecture
                        if (page.platform[i].data[j].arch) {
                          // x32
                          if (page.platform[i].data[j].arch.includes("x32")) {
                            $("input." + lower_env + "_arch32_platform_rec_chk[type='checkbox']").last().prop("checked", true);
                          } else {
                            $("input." + lower_env + "_arch32_platform_rec_chk[type='checkbox']").last().prop("checked", false);
                          }
                          // x64
                          if (page.platform[i].data[j].arch.includes("x64")) {
                            $("input." + lower_env + "_arch64_platform_rec_chk[type='checkbox']").last().prop("checked", true);
                          } else {
                            $("input." + lower_env + "_arch64_platform_rec_chk[type='checkbox']").last().prop("checked", false);
                          }
                        }
                      } /*else if (lower_env === "browser") {
                        if (page.platform[i].data[j].extension) {
                          $("input." + lower_env + "_extension_platform_rec_chk[type='checkbox']").last().prop("checked", true);
                          console.log(page.platform[i].data[j].extension);
                        } else {
                          $("input." + lower_env + "_extension_platform_rec_chk[type='checkbox']").last().prop("checked", false);
                        }
                      }*/
                    }
                  } else {
                    //$("input." + lower_env + "_platform_chk").prop("checked", false); // it unticks as it comes to else when i!=0 (for example tensorflow)
                  }
                
                }
                
                for (let i = 0, len = Object.keys(page.platform).length-1; i <= len; i++) {
                  {{ range $platform_list }}
                  platform_read_func(i, "{{ lower . }}", "{{ . }}");
                  {{ end }}
                }
              }
              if (page.categories) {
                $("#categories_chk").prop("checked", true);
                $("#categories_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                for(let i = 0, len = page.categories.length-1; i <= len; i++) {
                  if ( i > 0 ) {
                    $(".infocard-categories button.categories_add_rec").trigger("click");
                  }
                  $(".infocard-categories input.categories").last().val(page.categories[i]);
                }
              } else {
                $("#categories_chk").prop("checked", false);
              }
              if (page.aliases) {
                $("#aliases_chk").prop("checked", true);
                $("#aliases_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                for(let i = 0, len = page.aliases.length-1; i <= len; i++) {
                  if ( i > 0 ) {
                    $(".infocard-aliases button.aliases_add_rec").trigger("click");
                  }
                  $(".infocard-aliases input.aliases").last().val(page.aliases[i]);
                }
              } else {
                $("#aliases_chk").prop("checked", false);
              }
              if (page.license) {
                $("#license_chk").prop("checked", true);
                $("#license_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                for(let i = 0, len = page.license.length-1; i <= len; i++) {
                  if ( i > 0 ) {
                    $(".infocard-license button.license_add_rec").trigger("click");
                  }
                  $(".infocard-license input.license").last().val(page.license[i]);
                }
              } else {
                $("#license_chk").prop("checked", false);
              }
              if (page.social) {
                $("#social_chk").prop("checked", true);
                $("#social_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                for(let i = 0, len = page.social.length-1; i <= len; i++) {
                  if ( i > 0 ) {
                    $(".infocard-social button.social_add_rec").trigger("click");
                  }
                  $(".infocard-social input.name_social").last().val(page.social[i].name);
                  $(".infocard-social textarea.url_social").last().val(page.social[i].url);
                }
              } else {
                $("#social_chk").prop("checked", false);
              }
              if (page.quick_links) {
                $("#quick_links_chk").prop("checked", true);
                $("#quick_links_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                for(let i = 0, len = page.quick_links.length-1; i <= len; i++) {
                  if ( i > 0 ) {
                    $(".infocard-quick_links button.quick_links_add_rec").trigger("click");
                  }
                  $(".infocard-quick_links input.name_quick_links").last().val(page.quick_links[i].name);
                  $(".infocard-quick_links textarea.url_quick_links").last().val(page.quick_links[i].url);
                }
              } else {
                $("#quick_links_chk").prop("checked", false);
              }
              if (page.rating) {
                
                // local function expression
                // example use: 
                // remarks_and_src(i, page.source.rating, page.rating, "expert");
                let remarks_and_src = function(func_index = 0, func_rating_src = page.source.rating, func_rating = page.rating, func_type = "user") {
                  // following for tag appears as it is when radio button for expert rating is checked; with exception of word user/expert present on first line of next if tag only...
                  for (let j = 0, source_len = func_rating_src.length-1; j <= source_len; j++) {
                    // cache those DOM elements that are accessed more than once
                    let remarks = func_rating[func_index].remarks,
                        src_remarks = func_rating_src[j].remarks,
                        src_url = func_rating_src[j].url;
                    if (func_rating_src[j].name === func_rating[func_index].name && func_rating_src[j].type === func_type) {
                      if (remarks) {
                        $("input.remarks_rating").last().val(remarks);
                        if (src_remarks) {
                          if (remarks === src_remarks) {
                            $("textarea.src_rating").last().val(src_url);
                            $("input.src_rating_chk").last().prop("checked", true);
                            break;
                            // if break is not used then for loop continues over source.rating thus executes else on (current line number + 3) in all circumstances except one, where rating[name] and source.rating[name] matches at last iteration of the for loop
                          }
                        }
                      } else {
                        // remarks is undefined and so is src_remarks, i.e. they match (given that conditions in current if statement also match)
                        if (src_remarks === undefined) {
                          $("textarea.src_rating").last().val(src_url);
                          $("input.src_rating_chk").last().prop("checked", true);
                          break;
                          // if break is not used then for loop continues over source.rating thus executes else on line number 367 (current line number + 3) in all circumstances except one, where rating[name] and source.rating[name] matches at last iteration of the for loop
                        }
                      }
                    } else {
                      if (j === source_len) {
                        // we are unticking the checkbox on last iteration since it means that there is no match for page.rating and page.source.rating, implying there is no source for current entry in page.rating
                        $("input.src_rating_chk").last().prop("checked", false);
                      }
                    }
                  }
                }
                
                //
                $("#rating_chk").prop("checked", true);
                $("#rating_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                for(let i = 0, len = page.rating.length-1; i <= len; i++) {
                  if ( i > 0 ) {
                    $(".infocard-rating button.rating_add_rec").trigger("click");
                  }
                  $(".infocard-rating input.name_rating").last().val(page.rating[i].name);
                  $(".infocard-rating input.rate_rating").last().val(page.rating[i].rate[0]);
                  $(".infocard-rating input.outof_rating").last().val(page.rating[i].rate[1]);
                  if (page.rating[i].num) {
                    $(".infocard-rating input.users_rating_rdo").last().prop("checked", true);
                    $(".infocard-rating input.num_rating").last().val(page.rating[i].num);
                    remarks_and_src(i, page.source.rating, page.rating, "user");
                  } else {
                    $(".infocard-rating input.pro_rating_rdo").last().prop("checked", true);
                    remarks_and_src(i, page.source.rating, page.rating, "expert");
                  }
                }
              } else {
                $("#rating_chk").prop("checked", false);
              }
              if (page.note) {
                $("#note_chk").prop("checked", true);
                $("#note_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-note textarea.note").val(page.note);
              } else {
                $("#note_chk").prop("checked", false);
              }
              
              // reading source from here on
              if (page.source.sysreq) {
                for (let i = 0, source_len = Object.keys(page.source.sysreq).length-1; i <= source_len; i++) {
                  
                  for (let j = 0, source_len = Object.keys((page.source.sysreq)[i].data).length-1; j <= source_len; j++) {
                  
                    if ($(".sysreq_version").eq(i).find("textarea.src_sysreq").eq(j).parents(".sysreq_nav").find("input.sysreq_nav_name").eq(0).val() === ((page.source.sysreq)[i].data[j].title)) {

                    //if ($("textarea.src_sysreq").eq(i).parents(".sysreq_nav").find(".sysreq_nav_name").eq(0).val() === Object.keys(page.source.sysreq)[i]) {
                      $(".sysreq_version").eq(i).find("input.src_sysreq_chk").eq(j).prop("checked", true);
                      $(".sysreq_version").eq(i).find("textarea.src_sysreq").eq(j).val((page.source.sysreq)[i].data[j].data.join(", "));
                    } else {
                      alert("Markdown file you are trying to import has some synax problem(s) as: sysreq's order and its source's order do not match.");
                    }
                  }
                }
              } else {
                $("input.src_sysreq_chk").prop("checked", false);
              }
              if (page.source.platform) {
                
                let read_src_platform = function(i, lower_env, env) {
                  if (page.source.platform[i].hardware === env) {
                    $("input.src_" + lower_env + "_platform_chk").prop("checked", true);
                    $("textarea.src_" + lower_env + "_platform").eq(0).prop("disabled", false);
                    $("textarea.src_" + lower_env + "_platform").val(page.source.platform[i].data.join(", "));
                  }
                }
                for (let i = 0, source_len = page.source.platform.length-1; i <= source_len; i++) {
                  
                  {{ range $platform_list }}
                  read_src_platform(i, "{{ lower . }}", "{{ . }}");
                  {{ end }}
                  
                }
              }
              if (page.source.status) {
                $("input.src_status_chk").prop("checked", true);
                $("input.src_status_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-status textarea.src_status").val(page.source.status.join(", "));
              } else {
                $("input.src_status_chk").prop("checked", false);
              }
              if (page.source.features) {
                $("input.src_features_chk").prop("checked", true);
                $("input.src_features_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-features textarea.src_features").val(page.source.features.join(", "));
              } else {
                $("input.src_features_chk").prop("checked", false);
              }
              if (page.source.developer) {
                $("input.src_developer_chk").prop("checked", true);
                $("input.src_developer_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-developer textarea.src_developer").val(page.source.developer.join(", "));
              } else {
                $("input.src_developer_chk").prop("checked", false);
              }
              if (page.source.initial_release) {
                $("input.src_initial_release_chk").prop("checked", true);
                $("input.src_initial_release_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-initial_release textarea.src_initial_release").val(page.source.initial_release.join(", "));
              } else {
                $("input.src_initial_release_chk").prop("checked", false);
              }
              if (page.source.written_in) {
                $("input.src_written_in_chk").prop("checked", true);
                $("input.src_written_in_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-written_in textarea.src_written_in").val(page.source.written_in.join(", "));
              } else {
                $("input.src_written_in_chk").prop("checked", false);
              }
              if (page.source.license) {
                $("input.src_license_chk").prop("checked", true);
                $("input.src_license_chk").closest(".sf-component").find("input, button, textarea").prop("disabled", false);

                $(".infocard-license textarea.src_license").val(page.source.license.join(", "));
              } else {
                $("input.src_license_chk").prop("checked", false);
              }
              // till here //
            } else {
              console.log("We reached our target server, but it returned an error");
            }
          };
          request.onerror = function() {
            console.log("There was a connection error of some sort");
          };
          request.send();
        } catch (e) {
          console.log(e);
        }
      }
      reader.readAsDataURL(input.files[0]);
    }
    // When the user clicks on the button, scroll to the top of the document
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }
  $("input.filepath[type=file]").on("change", function() {
    readURL(this);
  });


  // writing to a file with .md extension when clicked on 'Save to File'
  $("button.save").on("click", function() {
    if (src_features_present() || src_developer_present() || src_initial_release_present() || src_written_in_present() || src_platform_present() || src_sysreq_present() || src_license_present() || src_rating_present() || src_status_present()) {
      src_present = true;
    }
    saveFormAsTextFile();
  });
  
});

