* Primary:
  1. Input
     * type: text
     * Notes: used in href attribute of anchor tag for email sharing

  2. Description
  
     `description` is very basic introduction of the software concerned in as few words as possible. `description` doesn't require a source, which itself might give an idea as to how general and short `description` should be. `description` appears on the search page and on the `softpage`, in both cases in very vicinity of the `title`. It should include the name of software, links for download of which, are available on current softpage. It is usually case that `description` conveys meaning about software by mentioning its categories, but this is not compulsory and will not apply to every case. Also, `description` is used in href attribute of anchor tag for email sharing. Hence in case HTML markup is used in `description` and there is need to use quotes for, say, HTML attribute, please make use of single quotes only. 

  3. Example
     1. [AForge.NET](/software/aforge.net/)
     2. [Apache Mahout](/software/apache-mahout/)

* Source: N/A
