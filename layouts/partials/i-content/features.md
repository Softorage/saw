* Primary:
  1. Input
     * type: text
     * Notes: Markdown supported

  2. Description
  
     1. `overview` is an overview of how and what the software does. It need not be extremely detailed and should not be lengthy, though shall be informative. Also, only absolutely necessary adjectives shall be used, and their use must not make `overview` appear as a marketing message.
    
     2. When name of a software is mentioned in `overview`, it shall be hyperlinked in markdown syntax as follows:
    
        `[software name](/software/software-perma/)`  
        where software-perma => software name with all spaces replaced with hyphens (\-), remove all round brackets and change all letters to small case.  
        
        `[category name](/categories/category-perma)`  
        where category-perma => category name with all spaces replaced with hyphens (\-), remove all round brackets and change all letters to small case.  
        
        Note: In case there appears name of a software that is not yet available in Softorage, hyperlink the name as per above format and add it to `priority software` to be added.
    
     3. In case you have copied content from a website, please quote the content and provide the source. To quote, insert `> ` at start of each line, and `> \- [Source](URL)` after the last line. See source file of [ELKI](/software/ELKI/) ([source-file](https://gitlab.com/Softorage/softorage.gitlab.io/blob/master/_software/elki.md)) or [Eclipse Deeplearning4j](/software/eclipse-deeplearning4j/) ([source-file](https://gitlab.com/Softorage/softorage.gitlab.io/blob/master/_software/eclipse-deeplearning4j.md)) for example.
     
        Note: In case you have quoted other source, do not hyperlink name or category of a software present in quoted text. You may/should include hyperlinks in quoted text as they exist in text from original source. Please make efforts to ensure that quoted text conforms to text from its original source. Also, it is suggested to include names of software present in quoted text, in next sentence on another line. See `overview` of [SciPy](/software/scipy-library/) for example.
    
     4. At the end of `overview`, tags shall be provided as given here-under. `source` shall not contain any source for these tags; tags shall not accompany source. Tags are separated by I(capital i) and must not be separated by a pipe(\|) or l(small L) or anything else. It is because pipe is used to create table in markdown and also ensures uniformity. (Although, only `overview` and `note` are markdown enabled, and you don't need to worry about using pipe in other `characteristics`, it is recommended that you use I(capital i) as a separator). Tags shall be hyperlinked using markdown syntax `[tag](https://hyperlink)`. A few examples of tags are given below.
        
        Blog  I  News  I  Forum  I  Wiki  I  Documentation  I  Mailing lists  I  FAQ  I  IRC  I  etc
  
        Tags shall not be named exactly as in examples. They shall be named as qualifies for the given situation. For example, say a software project has two FAQs, user and developer. Then the tags shall be `User FAQ` and `Developer FAQ`.
        Also in case you want to use a separator in tag name, you should use I(capital i) and must not use pipe(\|), as it breaks anchor tag. Pipe(\|) should be used only when a table is to be formed. Refer [Markdown Guide](https://www.markdownguide.org/extended-syntax/#tables) for more info.


* Source:
  1. Input
     * type: text
     * Notes: any number of words supported, `comma` separates `input-value` into distinct entities forming an array of strings

  2. Description
  
     For each of statements in `overview`, which are specific in nature, there shall be provided [acceptable `source`](/get-involved/#source). It should be noted that there shall not be provided source for tags specified at the end of `overview`, like for *from where the info about tags is collected*. Also, if using Wikipedia as source, it should be ensured that permanent link(can be found under 'Tools' on the Wikipedia page's sidebar) is used.

  3. Example:
     1. src1, src2 => 2 members of one array
