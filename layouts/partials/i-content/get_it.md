* Primary:
  1. Input
     * type:
       1. Name: text
       2. URL: URL
     * Notes: none

  2. Description
  
     It is usually found that software's official website provides instructions or links to get the software up and running on the system for all the platforms for which the software is available. In such case, URL of such download page be matched with `input-Name-option` : `Authentic`. URL matched with `input-Name-option` : `Authentic`, is shown in separate colour than other URLs on the `softpage`. 
     Sometimes a software is also released on say 'Github Releases', 'GitLab Releases', etc by the developer her/him/itself. In such cases, where the developer her/him/itself has released the software on different place(s) than official website, ` | Authentic` (`<space><bar><space>Authentic`) shall be included in the `input-value` for 'Name' at the end of name of the website. 
     In case there is no one specific page (from official website) where software for all officially supported packages can be found, then multiple records shall be created, with names of platform (if they are available platformwise) or in such way as seems logical in case. In case official website provides multiple pages for its different packages of same software (say each for different platforms), then name shall be suffixed by ` | Authentic` (`<space><bar><space>Authentic`), followed by a logical name(here, platform name).
  
     Rule of thumb: only in case and in all cases, where URL is of official website for the software, word `Authentic` be used.
  
     Even in case `Authentic` page covers instructions or provides links to software for all supported platforms, other websites providing download services or such may be included.
     
     It may be noted that no `source` is required.
     
  3. Example
      Here is an extract from [winrar.md](https://gitlab.com/Softorage/softorage.gitlab.io/blob/master/content/software/winrar.md)
      ```
      get_it:
        - from: "Authentic"
          url: "https://www.win-rar.com/download.html?L=0"
        - from: "FileHippo x32 (Windows)"
          url: "https://filehippo.com/download_winrar_32/"
        - from: "FileHippo x64 (Windows)"
          url: "https://filehippo.com/download_winrar_64/"
        - from: "TechSpot (All)" # since it has download links for all platforms
          url: "https://www.techspot.com/downloads/67-winrar.html"
        - from: "CNET x64 (Windows)"
          url: "https://download.cnet.com/WinRAR-64-bit/3000-2250_4-10965579.html"
        - from: "FileHorse x64 (Windows)"
          url: "https://www.filehorse.com/download-winrar-64/"
        - from: "Uptodown x64 (Windows)"
          url: "https://winrar.en.uptodown.com/windows"
        - from: "Softonic x32 (Windows)"
          url: "https://winrar.en.softonic.com/download"
        - from: "Softonic x64 (Windows)"
          url: "https://winrar-64bit.en.softonic.com/download"
        - from: "Google Play Store (Android)"
          url: "https://play.google.com/store/apps/details?id=com.rarlab.rar&hl=en_IN"
        - from: "Uptodown (Android)"
          url: "https://rar-for-android.en.uptodown.com/android"
      ```

* Platform:
  1. Input
     * type:
       1. Name: text
       2. Architecture: checkbox (x32/x64)
       3. Type: checkbox (Official/Unofficial)
       4. {+}: button
     * Notes: none

  2. Description
  
     In `input-field` : 'Name', name of platform shall be included. It takes following values: 1.Windows 2.Linux 3.macOS 4.Android 5.iOS 6.Any other
     
     **Note: Leave the 'Name' field empty for a platform pair and such pair shall not be counted in output. Very necessary for 'Authentic' link, and links with ` | Authentic`.**
     
     For 'architecture', appropriate checkbox shall be checked. It usually relevant only in cases of desktop platforms, like Windows, Linux, etc. Hence, in case of mobile platforms, it is allowed if no checkbox if checked.
     
     Sometimes, community releases a spin-off version of a certain software. Such a community version is usually termed as an unofficial version of software. Hence for 'type', checkbox for 'official' shall be unchecked only if there is available for download, a/the community version or such unofficial version of software, on the website (`input-field` 'Name' in Primary above).
     
     If portable version is available, tick the checkbox for portable.
     
     The {+} button: after pressing the button, another pair of input fields is added for platform. Do note that there is no button to remove an added pair of input fields, so be careful. In any case, if, by mistake, there have been added multiple pairs which are not needed, then leave the 'Name' field empty for that platform pair and such pair shall not be counted in output. For a website, platform field is included if it has only one platform pair with non-empty 'Name' field or if there are multiple platform pairs. __So, note that if a website doesn't need the platform field at all, {+} button *must* not be clicked. Very necessary for 'Authentic' link.__
     
  3. Example
     Say the website is CNET, and it has different links for x32 and x64 versions, and it has only Windows version for download. Then it shall follow nomenclature as:
     
     *  Name: CNET  
        
        Platform:  
          * for x32: 'Name': Windows, 'Architecture': x32 (checked), 'Type': Official  
        
        URL: https://.......  
      
     *  Name: CNET  
        
        Platform:  
          * for x64: 'Name': Windows, 'Architecture': x64 (checked), 'Type': Official  
        
        URL: https://.......  
     
     
     Say CNET (in our example) has same link for both x32 and x64 versions, i.e., it has links for download of both x32 and x64 versions on same page, then it shall follow nomenclature as:
     *  Name: CNET  
        
        Platform:   
          * for both x32 and x64: 'Name': Windows, 'Architecture': x32 (checked) / x64 (checked), 'Type': Official  
        
        URL: https://.......  
     
     For no information as to architecture:
     *  Platform:
          * 'Name': Windows, *'Architecture': (both unchecked)*, 'Type': Official

* Source: N/A
