* Primary:
  1. Input
     * type: text
     * Notes: format to followed: DD Month-name YYYY

  2. Description
  
     The date on which first version was released. It need not be v1 or equivalent, though it shall be the first stable version released, meaning that it must not be a beta, alpha or such. Special attention is to be given to format: DD Month-name YYYY (an update is likely in future that makes it easy to follow the format).

* Source:
  1. Input
     * type: text
     * Notes: any number of words supported, `comma` separates `input-value` into distinct entities forming an array of strings
  
  2. Description
    
     In many cases, date of initial release is present on Wikipedia or such source but no firm source can be found. A trick to find source for initial release date of software when such date is known, is to search in a search engine, the name of the software along with given date as the keyword. For example, if a software with name 'SoftySoft' has date of initial release as 25 Sept 2005 (which is present on Wikipedia), then keyword for search would be 'SoftySoft 25 Sept 2005'.

  3. Example:
     1. src1, src2 => 2 members of one array
