* Primary:
  1. Input
     * type: text
     * Notes: none

  2. Description
  
     Under which license(s) is the software released. Please make use of standard licenses in `input-options` provided. In case the license of software is a modified version of a standard license, then add `(m)` at the end of license name. See source file of [XeTeX](/software/xetex/) ([source file](https://gitlab.com/Softorage/softorage.gitlab.io/blob/master/_software/xetex.md)) for example.

* Source:
  1. Input
     * type: text
     * Notes: any number of words supported, `comma` separates `input-value` into distinct entities forming an array of strings

  2. Example:
     1. src1, src2 => 2 members of one array
