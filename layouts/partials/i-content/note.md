* Primary:
  1. Input
     * type: text
     * Notes: Markdown supported

  2. Description
  
     `note` shall include notes regarding any infocards (or `characteristics`), which need to be presented to give readers a clear picture. When `note` includes a mention of an `infocard` say System Requirements, then such `infocard` name shall be internally hyperlinked to `id` of concerned `infocard`. In current example, it will be `[System Requirements](#sysreq)`. A list of `id` for each `infocard` is given here-under:
     1. Description: description
     2. System Requirement: sysreq
     3. Platform: platform
     4. Developer: developer
     5. Initial Release: initial_release
     6. Repository: repository
     7. Written in: written_in
     8. Categories: categories
     9. License: license
     10. Social: social
     11. Rating: rating
  
     Also, in case there appears name of a software in `note`, hyperlink it to its `softpage` on invirds as follows:
     `[software name](/software/software-perma/)`  
     where software-perma => software name with all spaces replaced with hyphens (\-) and all letters in small case.  
     
     Although Softorage disclaims its use of software name, software logo and such related marks (read [disclaimer](/legal/disclaimer)), sometimes a software or its parent organisation may require cetain other conditions to be fulfilled, which may require further disclosure. In such case, `note` also includes notes relating to disclaimer. See [TensorFlow](/software/tensorflow/) for example. Such notices can be found on official website of such concerned software.

* Source: N/A
