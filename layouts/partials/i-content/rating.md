* Primary:
  1. Input
     * type:
       1. Name: text
       2. Rating: number
       3. Opinion type: radio button and number
       4. Remarks: text
     * Notes: none

  2. Description
  
     Name is the name of reviewer in exactly the way reviewer wants it to be in. These names are usually their trademarks hence need to be spelled in exactly the same manner in which the reviewer wants others to with due regards to capital and lower case letters. It is safest to choose from list that occurs when clicked on text area.
     
     Rating is the rating given by reviewer to software concerned and is in form rating-given/out-of. Eg, 4/5.
     
     If rating by reviewer is calculated based on number of users' reviews, then upper radio button is selected and number of users that have given rating is `input-value` in given area.
     
     Remarks is the short info, usually 1 to 2 words, that would convey information to reader to enable him to understand the relevance of given rating. In some cases it might be that a software has a free version and a premium version, and may as well have different ratings for such different versions. In such case `input-value` for Remarks would be `Free Version` or it may be left blank for rating corresponding to free version and `Premium Version` for rating corresponding to premium version [See example 1]. In some cases it might be that a reviewer only reviews x64 bit Windows version, in which case `input-value` for Remarks would be `x64 bit Windows` [See example 2]. 
     separate x32, portable, windows, etc by a hyphen
     
     For sites with portable version only (say both 32/64) Portable x32/64
     For sites with portable version as well as Unportable version Un/Portable x32/64
     
  3. Example
      1. Here is an extract from [winrar.md](https://gitlab.com/Softorage/softorage.gitlab.io/blob/master/content/software/winrar.md):
        ```
          rating:
            - name: "FileHippo"
              rate: [8, 10]
              num: 722
              remarks: "x32 bit Windows"
            - name: "TechSpot"
              rate: [4.5, 5]
              num: 87
            - name: "Softonic"
              rate: [8, 10]
              num: 25710
              remarks: "x32 bit Windows"
            - name: "Softonic"
              rate: [8, 10]
              num: 3223
              remarks: "x64 bit Windows"
            - name: "CNET"
              rate: [4.5, 5]
              num: 192
              remarks: "x64 bit Windows"
            - name: "CNET"
              rate: [4, 5]
              remarks: "x64 bit Windows"
            - name: "Uptodown"
              rate: [4, 5]
              num: 30
              remarks: "x64 bit Windows"
            - name: "FileHorse"
              rate: [7.1, 10]
              num: 521
              remarks: "x64 bit Windows"
            - name: "Softpedia"
              rate: [4.5, 5]
              num: 2406
            - name: "Uptodown"
              rate: [4.9, 5]
              num: 23
              remarks: "Android"
            - name: "Google Play"
              rate: [4.4, 5]
              num: 726788
              remarks: "Android"
        ```
      2. Here is an extract from [ccleaner.md](https://gitlab.com/Softorage/softorage.gitlab.io/blob/master/content/software/ccleaner.md):
        ```
          rating:
            - name: "TechRadar"
              type: "expert"
              url: "https://www.techradar.com/in/reviews/piriform-ccleaner-professional"
              remarks: "Professional Version"
        ```

* Source: 
  1. Input
     * type: text/URL
     * Notes: any number of words supported, `comma` does **not** separate `input-value` into distinct entities forming an array of strings

  2. Description
    
     It is the source usually URL from where the rating has been obtained. If source is not available for a rating, then text area given for source should be left blank and checkbox next to it shall be unticked.
