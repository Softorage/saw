* Primary:
  1. Input
     * type:
       1. Name: text
       2. URL: URL
     * Notes: none

  2. Description
  
     Say a software has social presence across 3 websites(say Facebook, Twitter and Wikipedia). Then there would be 3 records, each for one website where the software has its social presence. `Name column` gets name of the website where software has its social presence and `URL column` gets the URL of software's page on such website. For `Name column`, please choose from `input-Name-options` provided. Wikipedia page should not be *permanent link*.

* Source: N/A