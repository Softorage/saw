* Primary:
  1. Input
     * type:
       1. Minimum: text
       2. Recommended: text
       3. Optimum: text
     * Notes: tick the checkbox of only the required column(s)

  2. Description
  
     In case there are different system requirements for a software under different operating systems, then use I(capital i) to differentiate them in single row. see [The Microsoft Cognitive Toolkit](/software/the-microsoft-cognitive-toolkit/) for example.
     
     Note: Ensure that there doesn't appear a newline in `input-value` for any `input-field`, as it breaks the output.

* Source:
  1. Input
     * type: text
     * Notes: any number of words supported, `comma` separates `input-value` into distinct entities forming an array of strings

  2. Description
  
     From where did you collect the info presented herein.

  3. Example:
     1. src1, src2 => 2 members of one array
