* Primary:
  1. Input
     * type: text
     * Notes: also decides the filename and the URL for the software

  2. Description
  
     `title` is basically the name of software. It also decides the filename and the URL for the software under consideration. It should however be noted that `title` is not just a name of the software, hence is case-sensetive. `title` must appear the exact same way as it appears on the premises of software whether online or offline. For example, darktable is a Photo post-Production software. On its official website, darktable is written as darktable, hence its `title` is darktable and not Darktable or DarkTable or else. In case where `title` of a software appears in different ways on its own official premises, then that of it shall form `title` which is more frequent. For example, a software's `title` on the homepage of its official website and as it appears on the title of the same page(as the title of that tab or window of browser) differs. If no specific form is more frequent, then a choice needs to be made, which shall usually favour gramatically correct way.
     Also, in case the software is usually marketed along with the name of organisation behind it, then such name shall form `title` (See example 3, 4).
     If a software name is likely to create confusion with other names, or software name makes more sense when accompanied with a relevant general name, such general name shall be written in brackets (see example 5, 6).

  3. Example
     1. darktable
     2. SciPy
     3. Apache Mahout
     4. Eclipse Deeplearning4j
     5. Brave (Web Browser)
     6. Massive Online Analysis (MOA)

* Source: N/A
