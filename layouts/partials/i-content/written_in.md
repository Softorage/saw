* Primary:
  1. Input
     * type: text
     * Notes: none

  2. Description
  
     In which computer languages is the software written. Please make use of `input-options` provided.

* Source:
  1. Input
     * type: text
     * Notes: any number of words supported, `comma` separates `input-value` into distinct entities forming an array of strings

  2. Description
  
     Usually a webpage which presents a persuasive proof of the information presented here. For example, software repository's page which lists info about computer languages used in software source code. eg, `https://github.com/****/****/`, `https://gitlab.com/****/****/graphs/master/charts`. If no such page can be found, then a source like Wikipedia page(*permanent link* only) may as well be used.

  3. Example:
     1. src1, src2 => 2 members of one array